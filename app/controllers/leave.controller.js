const UserModel = require('../models/user.model');
const LeaveStoreModel = require('../models/leaveStore.model');
const TranscationModel = require('../models/transaction.model');
const moment = require('moment');
var NowDate = moment().format("YYYY-MM-DD");
var year = new Date().getFullYear();
var years = year + "-" + (year + 1);
var month = moment().format("MMM");

function getDates(startDate, stopDate) {
    var dateArray = [];
    var currentDate = moment(startDate);
    var stopDate = moment(stopDate);
    while (currentDate <= stopDate) {
        dateArray.push(moment(currentDate).format('YYYY-MM-DD'))
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
}

function validateDates(id, name, fromdate, toDate) {
    var flag;
    return new Promise((resolve) => {
        TranscationModel.find({ id: id, name: name, year: years })
            .then(transactions => {
                if (transactions.length == 0) {
                    resolve(true);
                } else {
                    for (var i = 0; i < transactions.length; i++) {
                        for (var j = 0; j < transactions[i].dates.length; j++) {
                            if (fromdate == transactions[i].dates[j] || toDate == transactions[i].dates[j]) {
                                flag = false;
                                resolve(false);
                            } else {
                                flag = true;
                            }
                        }
                    }
                    if (flag == true) { resolve(true) }
                }
            }).catch(err => {
                resolve(err);
            })
    });

}


exports.applyLeave = async (req, res) => {
    var index;

    //#region req.body
    const {
        id = id,
        name = name,
        fromDate = fromDate,
        toDate = toDate,
        reason = reason,
        typeOfLeave = typeOfLeave,
        fromDatePartOfDay = fromDatePartOfDay,
        toDatePartOfDay = toDatePartOfDay,
        description = description
    } = req.body;
    //#endregion req.body

    var Dates = getDates(fromDate, toDate); Dates = getDates(fromDate, toDate);
    var validatedDates = await validateDates(id, name, fromDate, toDate).then(value => value);

    if (validatedDates == false) {
        return res.jsonp({ message: "Leaves have been applied for this dates,Please choose different dates" });
    } else {
        //#region MainCode
        if (fromDate >= NowDate && toDate >= NowDate) {
            await LeaveStoreModel.findOne({
                id: id,
                name: name,
                year: years
            })
                .then(async result => {
                    if (result === null) {
                        return res.jsonp({ message: "User not found" });
                    } else {
                        var mon = result.store.attendence;
                        for (var i = 0; i < mon.length; i++) {
                            if (month == mon[i].month) {
                                index = i;
                                var plBalance = mon[i].plBalance;
                                var balance = plBalance - Dates.length;

                                //#region Transcations
                                const Transaction = new TranscationModel({
                                    id: result.id,
                                    name: result.name,
                                    year: years,
                                    fromDate: fromDate,
                                    toDate: toDate,
                                    dates: Dates,
                                    reason: reason,
                                    typeOfLeave: typeOfLeave,
                                    fromDatePartOfDay: fromDatePartOfDay,
                                    toDatePartOfDay: toDatePartOfDay,
                                    totalDays: Dates.length,
                                    description: description,
                                    balanceAfterTransaction: balance
                                })
                                //#endregion Transcations

                                await Transaction.save()
                                    .then(async transaction => {
                                        await LeaveStoreModel.findOneAndUpdate({
                                            id: id, name: name, year: years,
                                            "store.attendence.month": month
                                        }, { $set: { "store.attendence.$.plBalance": balance } })
                                            .then(updatedLeaveStore => { res.jsonp({ Message: "success" }) })
                                            .catch(err => { return res.jsonp(err) })
                                    }).catch(err => { return res.jsonp(err); })
                            }
                        }
                    }
                }).catch(err => { return res.jsonp(err) })
        } else {
            return res.jsonp({ message: "Invalid Dates" });
        }
        //#endregion MainCode   
    }
}



/*
1)check if leave is PL,CL or SL
2)Maintain total balance of PL+CL+SL
3)substact leaves from specific leave type
4)
*/