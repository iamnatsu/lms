const UserModel = require('../models/user.model');
const LeaveStoreModel = require('../models/leaveStore.model');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const Attendencejson = require("../../util/data");
var date = new Date().getFullYear();
var year = date + "-" + (date + 1);

//#region Encryption function
var genRandomString = function (length) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length); /** return required number of characters */
};
var sha512 = function (password, salt) {
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

function saltHashPassword(userpassword) {
    var salt = genRandomString(16); /** Gives us salt of length 16 */
    var passwordData = sha512(userpassword, salt);
    return ({
        passwordHash: passwordData.passwordHash,
        salt: passwordData.salt
    });
}

//#endregion Encryption function


exports.createUser = async (req, res) => {
    //#region req.body
    const { name, team, designation, role, manager, joiningDate, email, password } = req.body;
    //#endregion req.body

    if (!name && role && joiningDate && email && password) {
        return res.status(400).send({ message: "User Details are missing" });
    }

    await UserModel.findOne({ name: name }, async (err, user) => {
        if (err) { return err; }
        if (!user) {
            var encyptedPassword = JSON.stringify(saltHashPassword(password));
            const user = new UserModel({
                name: name,
                team: team,
                designation: designation,
                role: role,
                manager: manager,
                joiningDate: joiningDate,
                email: email,
                password: encyptedPassword
            });
            await user.save()
                .then(async user => {
                    const leaveStore = new LeaveStoreModel({
                        id: user._id,
                        name: user.name,
                        year: year,
                        store: Attendencejson
                    })
                    await leaveStore.save()
                        .then(leaveStore => {
                            res.status(200).send({
                                message: "success",
                                user,
                                leaveStore
                            });
                        }).catch(err => { res.status(500).send({ message: err }) });
                }).catch(err => { res.status(500).send({ message: err }) });
        } else {
            return res.jsonp({ message: "User Already Exits" });
        }
    })
};

exports.login = async (req, res) => {
    var email = req.body.email || '';
    var password = req.body.password || '';

    if (email == '' || password == '') {
        res.status(401);
        res.jsonp({
            "Message": "email or password is empty"
        });
        return;
    }

    var dbUserObject = await validate(email, password);
    console.log(dbUserObject);
    if (!dbUserObject.success) {
        res.status(401);
        res.jsonp({
            "Result": dbUserObject
        });
        return;
    }
    if (dbUserObject.success) {
        res.jsonp(dbUserObject);
    }
}

const validate = (email, password) => {
    var email = email;
    var pass = password;
    return new Promise(function (resolve) {
        UserModel.find({
            email: email
        }).then(async (result) => {
            if (result.length == 0) {
                resolve({
                    success: false,
                    Result: "User does not exit, Create a new User"
                })
            } else {
                var Resultpass = JSON.parse(result[0].password);
                var decyptedPassword = await sha512(pass, Resultpass.salt);
                if (Resultpass.passwordHash == decyptedPassword.passwordHash) {
                    const token = genToken({
                        email: email
                    })
                    resolve({
                        statuscode: 200,
                        success: true,
                        token: token
                    })
                } else {
                    resolve({
                        success: false,
                        message: 'Username or password is Incorrect'
                    })
                }
            }
        }).catch(err => {
            resolve({
                success: false,
                Result: err
            })
        })
    })
}

const genToken = (user) => {

    var token = jwt.sign({
        username: user.username
    }, `"process.env.secret_key"`, {
            expiresIn: 86400
        });
    return token;
}