const UserModel = require('../models/user.model');
const LeaveStoreModel = require('../models/leaveStore.model');
const TransactionModel = require('../models/transaction.model');


exports.getAllUser = (req, res) => {
    UserModel.find().then(result => {
        res.jsonp(result);
    })
}

exports.LeaveStore = (req, res) => {
    LeaveStoreModel.findOne({
        name: req.params.name
    }).then(result => {
        res.jsonp(result);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Error occured while creating user"
        });
    })
}

// var frommDate = "2019-09-18"
// TransactionModel.find({id:"5d6962a4c186423bfcda7c2c",name:"Rohan",year:"2019-2020"})
// .then(transactions =>{
//     for(var i=0;i< transactions.length;i++){
//         for(var j=0;j< transactions[i].dates.length;j++){
//             if(frommDate==transactions[i].dates[j]){
//             return console.log(transactions[i].dates[j])
//         }
//     }
// }
//         // syncLoop(transactions.length, function (Mainloop) {
//     //     var i = Mainloop.iteration(); 
//     //        syncLoop(transactions[i].dates.length, function(innerloop){
//     //            var j= innerloop.iteration();
//     //            if(frommDate==transactions[i].dates[j]){
//     //              return console.log(transactions[i].dates[j])
//     //            }
//     //         innerloop.next();
//     //        }),function(){}
//     //     Mainloop.next();
//     //   }, function () {
//     //     console.log("finish mainloop");
//     //   });
// })

