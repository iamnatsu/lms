var express = require("express");
var router = express.Router();
const userController = require('../controllers/user.controller');
const LeaveController = require('../controllers/leave.controller');
const LoginController = require('../controllers/login.controller');

//#region userController
router.get('/get', userController.getAllUser);
router.get('/leaves/:name', userController.LeaveStore);
//#endregion userController

//#region LeaveController
router.post('/applyLeave', LeaveController.applyLeave);
//#endregion LeaveController

//#region login
router.post('/createUser', LoginController.createUser);
router.post('/login', LoginController.login);
//#endregion login


module.exports = router;