const mongoose = require('mongoose');

const Transaction = mongoose.Schema({
    id: String,
    name: String,
    year: String,
    fromDate: Date,
    toDate: Date,
    dates: [],
    reason: String,
    typeOfLeave: String,
    fromDatePartOfDay: Date,
    toDatePartOfDay: Date,
    totalDays: String,
    description: String,
    balanceAfterTransaction: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Transaction', Transaction);