const mongoose = require('mongoose');

const User = mongoose.Schema({
    name: String,
    team: String,
    designation: String,
    role: String,
    manager: String,
    joiningDate: Date,
    isApplicableForLeave: Boolean,
    email: String,
    password: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('user1', User);