const mongoose = require('mongoose');

const LeaveStore = mongoose.Schema({ // creating table schema
    id: String,
    name: String,
    year: String,
    store: {}
}, {
    timestamps: true
});

module.exports = mongoose.model('leavestore', LeaveStore); // defining table name