var CronJob = require('cron').CronJob;
const UserModel = require('../app/models/user.model');
const LeaveStoreModel = require('../app/models/leaveStore.model');
const Attendencejson = require("../util/data");
var date = new Date().getFullYear();
var year = date + "-" + (date + 1);

new CronJob('1 * * * * *', async function () {
    //0 0 1 4 * every year april 1st
    await UserModel.find().then(async result => {
        console.log(result.length);
        for (var i = 0; i < result.length; i++) {
            const leaveStore = new LeaveStoreModel({
                id: result[i]._id,
                name: result[i].name,
                year: year,
                store: Attendencejson
            })
            await leaveStore.save()
                .then(console.log("Data Inserted of leavestore"))
                .catch(err => {
                    return res.status(500).send({
                        message: err.message || "Error occured while creating user"
                    });
                })
        }
    })
    console.log("Every minute");
}, null, true);

var CronJob = require('cron').CronJob;
new CronJob('* * * * * *', function () {
    console.log('You will see this message every second');
}, null, true, 'America/Los_Angeles');