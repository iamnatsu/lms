const express = require('express');
const bodyParser = require('body-parser');
const dbConfig = require('./util/db');
const mongoose = require('mongoose');
const cors = require('cors');
const moment = require('moment');
const config = require('dotenv').config();
const app = express();
const router = require('./app/routes/user.routes');
// const cron = require('./services/cron');

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())
app.use(cors());


// Routes
app.use('/', router);
// Routes



app.listen(8000, () => {
    console.log("App is runing on port 8000");
})

module.exports = app;